const io = require('../io');

const requestJson = require("request-json");
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuaeb10ed/collections/";
const mLabAPIKey = "apiKey=hL16BmgU-p0pPonKli-AY1WGd8dix6_Y";



function getUsersV1(req, res) {

 console.log("GET /apitechu/v1/users");  var result = {};

 var users = require('../usuarios.json');  if (req.query.$count == "true") {

   console.log("Count needed");

   result.count = users.length;

 }  result.users = req.query.$top ?

   users.slice(0, req.query.$top) : users;  res.send(result);

}

// le meto la GetUser V2 que enlazo con Postman para conectar la http dela API con el mLab
function getUsersV2(req, res) {
 console.log("GET /apitechu/v2/users");
// no vale con crear la libreria tambien hay que activarla.
//los clients se crean dentro de una URL Base createClient(URLBASE)





var httpClient = requestJson.createClient(baseMLabURL);
 console.log("Cliente creado");
//obtener toda la lista de usuarios. solo poner user y el apikey
 httpClient.get("user?" + mLabAPIKey,
 function(err, resMLab, body) {
  var response = !err ? body :{
  "msg" : "Error obteniendo usuarios"
}
 res.send(response);
}
)
}

//meto la consukta de vuelta
function getUserByIdV2(req, res) {
 console.log("GET /apitechu/v2/users:id");

 var id = req.params.id;
 var query = 'q={"id":' + id + '}';
 console.log("La consulta es" + query);

// crea cliente http
 var httpClient = requestJson.createClient(baseMLabURL);
 console.log("Client created");

 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (err) {
       var response = {
         "msg" : "Error obteniendo usuario"
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         var response = body[0];
       } else {
         var response = {
           "msg" : "Usuario no encontrado"
         }
         res.status(404);
       }
     }
     res.send(response);
   }
 )
}

var id = req.params.id;
var query = 'q{"id":' + id + '}';
console.log ("la consulta es" + query);

}



function createUserV1(req, res) {

 console.log("POST /apitechu/v1/users");  console.log(req.body.first_name);

 console.log(req.body.last_name);

 console.log(req.body.email);  var newUser = {

   "first_name": req.body.first_name,

   "last_name": req.body.last_name,

   "email": req.body.email

 }
 var users = require('../usuarios.json');

 users.push(newUser);

 io.writeUserDatatoFile(users);

 console.log("Usuario añadido con éxito");  res.send({"msg" : "Usuario añadido con éxito"})

}
function createUserV2 (req, res) {
  console.log("POST /apitechu/v2/users");
 console.log(req.body.id);
 console.log(req.body.first_name);
 console.log(req.body.last_name);
 console.log(req.body.email);
 console.log(req.body.password);
}
var newUser = {
  "id": req.body.id;
   "first_name": req.body.first_name;
   "last_name": req.body.last_name;
   "email": req.body.email;
   "password": req.body.password
}
//meto var http client
var httpClient = requestJson.createClient(baseMLabURL);
console.log("Client created");

httpClient.post("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    consolelog("usuario creado en mLab");
    res.status(201).send({"msg":"Usuario guardado"});
  )
}


function deleteUserV1(req, res) {

 console.log("DELETE /apitechu/v1/users/:id");

 console.log("id es " + req.params.id);  var users = require('../usuarios.json');

 var deleted = false;  console.log("Usando for of 2");

 // Destructuring, nodeJS v6+

 for (var [index, user] of users.entries()) {

   console.log("comparando " + user.id + " y " +  req.params.id);

   if (user.id == req.params.id) {

     console.log("La posicion " + index + " coincide");

     users.splice(index, 1);

     deleted = true;

     break;

   }

 }  if (deleted) {

   io.writeUserDataToFile(users);

 }
 var msg = deleted?
 "Usuario borrado" : "Usuario no encontrado."
 console.log(msg);

 res.send({"msg" : msg});

}

module.exports.getUsersV1 = getUsersV1;

module.exports.getUsersV1 = getUsersV2;

module.exports.getUserByIdV2 = getUserByIdV2;

module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;


module.exports.deleteUserV1 = deleteUserV1;
