// esto lo metere para hacer el ignore del bower:
//require("dotenv").config();
//


const express = require('express');
const app = express();

const port = process.env.PORT || 3000;


var enableCORS = function(req, res, next) {
res.set("Access-Control-Allow-Origin", "*");
res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

next();
}




app.use(express.json());
app.use(enableCORS());



const userController = require('./controllers/usercontroller');
const authController = require('./controllers/AuthController');


app.listen(port);
console.log("API escuchando en el puerto cambio2 " + port);

app.get('/apitechu/v1/hello',
function(req, res)  {
  console.log("GET /apitechu/v1/hello");
 res.send({"msg" :"Hola desde API TechU"});
}
)

//esto sobraria
//app.post('/apitechu/v1/users',AuthController.createUserV1UsersV1);
app.get('/apitechu/v2/users', userController.getUserV2);

// hasta aqui

// REGISTRO LAS RUTAS  V.IS
app.get('/apitechu/v1/users',userController.getUsersV1);
// registro la ruta de v2
app.get('/apitechu/v2/users',userController.getUsersV2);

app.post('/apitechu/v1/users',userController.createUsersV1);
app.post('/apitechu/v1/users',userController.createUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);

app.delete("/apitechu/v1/users/:id",userController.deleteUserV1);


// post a login
app.post('/apitechu/v1/login',authController.loginUsersV1);
// post a logout
app.post('/apitechu/v1/logout/:id',authController.logoutUsersV1);

app.post("/apitechu/v1/monstruo/:p1/:p2",
function (req, res) {
  console.log("Parámetros");
  console.log(req.params);

  console.log("Query String");
  console.log(req.query);

  console.log("Headers");
  console.log(req.headers);

  console.log("Body");
  console.log(req.body);

}
)

// fin V.IS

app.post('/apitechu/v1/users',
function(req, res) {
  console.log("POST /apitechu/v1/users");

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email
  }
//traemos fichero de usuarios a continuacion

  var users = require('./usuarios.json');
  users.push(newUser);
//invocamos a la funcion nueva
  writeUserDatatoFile(users);
  console.log("Usuario añadido con exito");
  res.send({"msg" : "Usuario añadido con exito"})

  }
)
// mi prueba para LOGIN
app.post('/apitechu/v1/usersLogin',
function(req, res) {
  console.log("POST /apitechu/v1/usersLogin");

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name":req.body.last_name,
    "email":req.body.email
  }
//traemos fichero de usuarios a continuacion

  var users = require('./usuarios.json');
  users.push(newUser);
//invocamos a la funcion nueva
  writeUserDatatoFile(users);
  console.log("Usuario añadido con exito");
  res.send({"msg" : "Usuario añadido con exito"})

  }
)

//Creamos funcion nueva borrado. el splice muta la lista
//(req.params) Checks route params, ex: /user/:id

app.delete("/apitechu/v1/users/for1/:id",
function(req, res){
  console.log("DELETE/apitechu/v1/users/for1/:id");
  console.log("Id es el " + req.params.id);
//traemos el fichero de usuarios
// tb podria definir un array var fruits = ["Banana", "Orange", "Apple", "Mango"];
  var users = require('./usuarios.json');
  var i;
  for (i = 0; i < users.length; i++)  {
    console.log("El indice vale" + i);
    console.log("Me quedan los siguientes usuarios en el array" + users.length);
//    length Sets or returns the number of elements in an array
     if (users[i].id == req.params.id) {
     console.log("parametro de la URL " + req.params.id);
 //    quiero borrar ese parametro
         console.log("paso por splice y el indice vale " + i);
         users.splice(i,1);
         writeUserDatatoFile(users);
         console.log("Usuario borrado");
         break; // se sale de aqui con el break
  }
};
res.send({"msg" : "Usuario añadido con exito"})
}
)







// meto la refactoriacion

function writeUserDatatoFile(data) {
 console.log("writeUserDatatoFile");  const fs = require('fs');
 var jsonUserData = JSON.stringify(data);  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
   function(err) {
     if(err) {
       console.log(err);
     } else {
       console.log("Usuario persistido");
     }
   }
 )
}











// desde aqui V.IS

//app.post('/apitechu/v1/users',userController.deleteUsersV1);
// funcion de creacion del usuario *****   POST ***
// el dia 22 donde pone headers pongo bodypara refatorizar
// app.post('/apitechu/v1/users',
//  function(req, res) {
//    console.log("POST /apitechu/v1/users");
//    console.log(req.body.first_name);
//    console.log(req.body.last_name);
//    console.log(req.body.email);
//
//    var newUser = {
//      "first_name": req.body.first_name,
//      "last_name":req.body.last_name,
//      "email":req.body.email
//    }
//  //traemos fichero de usuarios a continuacion
//
//    var users = require('./usuarios.json');
//    users.push(newUser);
// //invocamos a la funcion nueva
//    writeUserDatatoFile(users);
//    console.log("Usuario añadido con exito");
//    res.send({"msg" : "Usuario añadido con exito"})
//
//    }
// )
